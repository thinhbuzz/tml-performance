const request = require('request-promise');
const Promise = require('bluebird');
const Log = require('./log');


function call() {
    const log = new Log();
    return request({
        uri: 'http://tml-web-app.azurewebsites.net/api/getDevicesByAirlineCodeAz/VIE',
        method: 'GET',
        json: true
    })
        .then(() => {
            log.success();
            return Promise.resolve();
        })
        .catch(error => {
            console.error(error);
            log.error();
            return Promise.resolve();
        });
}

Promise.map(Array(100), item => call(), {concurrency: 1})
    .then(() => {
        console.info('done');
    })
    .catch((error) => {
        console.error('error', error);
    });
