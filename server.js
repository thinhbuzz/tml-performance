const express = require('express');
const request = require('request-promise');
const bodyParser = require('body-parser');
const Log = require('./log');
let LogModel;

if (process.env.WRITE_LOG_TEST) {
    LogModel = require('./models/log');
}

const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse application/json
app.use(bodyParser.json());

app.get('/', function (req, res) {
    return res.send('Hello World!');
});
app.post('/log', function (req, res) {
    return LogModel.create(req.body)
        .then(function () {
            return res.status(200).json({success: true});
        })
        .catch(function (error) {
            return res.status(500)
                .json((error && error.message) ? {error: error.message} : {error: 'Internal Error'});
        });

});
app.get('/api/getDevicesByAirlineCodeAz/:code', function (req, res) {
    const log = new Log();
    return request({
        uri: 'https://tml-function.azurewebsites.net/api/getAirlineByCode',
        qs: {
            code: 'va8tElpD2XqCJWfLe5aXkOuN8jQ65OqtEVjcaNlHlpWSrPzRbtDpmw==',
            airlineCode: req.params.code
        },
        json: true
    })
        .then(function (response) {
            log.success();
            res.json(response ? response : 'Can not get any airline info by ' + req.query.code + ' code here');
        })
        .catch(function (error) {
            log.error();
            res.status(500)
                .json((error && error.message) ? {error: error.message} : {error: 'Internal Error'});
        });
});

app.listen(process.env.PORT || 3000, function () {
    return console.log('Example app listening on port 3000!');
});
