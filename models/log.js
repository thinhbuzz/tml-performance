const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

mongoose.connect('mongodb://localhost/tml-logs');
const db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    console.info('Database connected!');
});

const LogSchema = mongoose.Schema({
    type: String,
    time: Number,
    result: String
}, {versionKey: false});

module.exports = mongoose.model('Log', LogSchema);
